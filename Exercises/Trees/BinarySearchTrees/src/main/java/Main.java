public class Main {
    public static void main(String[] args) {

        Tree tree01 = new Tree();
        tree01.insert(10);
        tree01.insert(7);
        tree01.insert(15);
        tree01.insert(20);
        tree01.insert(4);
        tree01.insert(11);
        tree01.insert(17);


        Tree tree = new Tree();
         tree.insert(7);
         tree.insert(4);
         tree.insert(9);
         tree.insert(1);
         tree.insert(6);
         tree.insert(8);
         tree.insert(10);
        System.out.println("Done");
        tree.traversePreOrder(tree.root);
       // System.out.println("factorial of 3 = " + factorial(3));
        System.out.println("In order");
        tree.traverseInorder(tree.root);
        System.out.println("Post Order");
        tree.traversePostOrder(tree.root);

        System.out.println("Height of tree is " + tree.height(tree.root));

        System.out.println("Minimun in the tree is " + tree.min(tree.root));

       // System.out.println("Are the trees equal " + tree.equals(tree01));
        System.out.println("Is Valid BFS " + tree.validate(tree.root));

    }

    public static  int factorial(int n) {
        if(n==1)
            return 1;
        else {
           return n * factorial(n-1);
        }
    }

    public boolean equals(Tree tree01) {
       return  true;
    }
}
