public class Tree {

    public Node root;

    public  class  Node {
        private int value;
        private Node leftChild;
        private Node rightChild;

        public  Node(int value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return "Node=" + value;
        }
    }

    public void  insert(int value) {

        Node node = new Node(value);

       if(root == null) {
           root = node;
           return;
       }
       Node current = root;

       while(true) {
           if (value < current.value) {
               if(current.leftChild == null) {
                   current.leftChild = node;
                   break;
               }
               current = current.leftChild;
           }
           else {
               if(current.rightChild == null) {
                   current.rightChild = node;
                   break;
               }
               current = current.rightChild;
           }
       }
    }

    public boolean find(int value, Node currentNode) {
        if(currentNode.value == value) return true;
        if(currentNode.value > value) {
            return find(value,currentNode.leftChild);
        }
        else {
            return find(value,currentNode.rightChild);
        }
    }

    public void traversePreOrder(Node root) {
        // visit root (print) then go left and the right
        if(root == null)
            return;
        System.out.println(root.value);
        traversePreOrder(root.leftChild);
        traversePreOrder(root.rightChild);
    }

    public void traverseInorder(Node root) {
        // visit root (print) then go left and the right
        if(root == null)
            return;
        traverseInorder(root.leftChild);
        System.out.println(root.value);
        traverseInorder(root.rightChild);
    }

    public void traversePostOrder(Node root) {
        // visit root (print) then go left and the right
        if(root == null)
            return;
        traversePostOrder(root.leftChild);
        traversePostOrder(root.rightChild);
        System.out.println(root.value);
    }

    private boolean isLeaf(Node node) {
        return  node.rightChild == null && node.leftChild == null;
    }

    public  int height(Node node) {
        if(node == null)
            return -1;
        if (isLeaf(node))
            return 0;
        return 1 + Math.max(height(node.leftChild), height(node.rightChild));
    }

    public int min(Node root) {
        if(isLeaf(root))
            return  root.value;
        int left = min(root.leftChild);
        int right = min(root.rightChild);
        return Math.min(Math.min(left, right), root.value);
    }

    public boolean equals(Tree other) {
        if(other == null)
            return false;
        return equal(root, other.root);
    }

    private boolean equal(Node first, Node second) {
        if(first == null && second == null)
            return true;
        if(first != null && second != null)
            return equal(second.leftChild, first.leftChild) && equal(second.rightChild, first.rightChild) && first.value == second.value;
        return false;
    }

    private void swap() {
        Node temp = root.rightChild;
        root.rightChild = root.leftChild;
        root.leftChild =temp;
    }

    public  boolean validate(Node node) {
        // swap();
        return validateBinarySearchTree(node, -9999999, 9999999);
    }

    private boolean validateBinarySearchTree(Node node, int minValue, int maxValue) {
      if(node == null)
        return true;
      if(node.value > maxValue || node.value < minValue)
          return false;
      return validateBinarySearchTree(node.leftChild, minValue, node.value -1)
              && validateBinarySearchTree(node.rightChild, node.value + 1, maxValue);
    }

}
